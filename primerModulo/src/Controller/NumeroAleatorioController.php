<?php

namespace Drupal\primerModulo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An Numero aleatorio
 */
class NumeroAleatorioController extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build =  [
      '#theme' => 'primerModulo',
      '#test_var' => rand(100,10000)),
    ];

  return $build;

  }

}
