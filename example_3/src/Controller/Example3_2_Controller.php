<?php

namespace Drupal\example_3\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class Example3_2_Controller extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build =  [
      '#theme' => 'example_3',
      '#test_var' => rand(100,10000),
    ];

  return $build;

  }

}
